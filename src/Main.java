/** Fork by Mike K. **/
import java.util.*;
import java.io.*;
import java.math.*;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class Player {

    public static void main(String args[]) {
        Random random = new Random();
        Scanner in = new Scanner(System.in);
        int[][] board = new int[3][3];

        // game loop
        while (true) {
            String move = getNextMove(in, random, board);
            System.out.println(move);
        }
    }

    public static String getNextMove(Scanner in, Random rnd, int[][] board) {
        int opponentRow = in.nextInt();
        int opponentCol = in.nextInt();
        int validActionCount = in.nextInt();
        List<String> validActions = new ArrayList<>(validActionCount);

        for (int i = 0; i < validActionCount; i++) {
            int row = in.nextInt();
            int col = in.nextInt();

            validActions.add(String.format("%d %d", row, col));
        }

        // Write an action using System.out.println()
        // To debug: System.err.println("Debug messages...");
        String nextMove = validActions.get(rnd.nextInt(validActions.size()) );
        return nextMove;
    }

    public static int[][] createStateFromAction(int player, String action, int[][] board) {
        String[] action_chars = action.split(" ");

        int row = Character.getNumericValue(action_chars[0].charAt(0));
        int col = Character.getNumericValue(action_chars[1].charAt(0));

        if (board[row][col] != 0) {
            throw new RuntimeException("This cell is already occupied!");
        }
        System.out.printf("%d %d", row, col);

        board[row][col] = player;
        return board;
    }

    public static int getWinner(int[][] board) {
        for (int i = 0; i < 3; i++) {
            if (board[0][i] != 0
                    && board[0][i] == board[1][i]
                    && board[0][i] == board[2][i])
                return board[0][i];
            if (board[i][0] != 0
                    && board[i][0] == board[i][1]
                    && board[i][0] == board[i][2])
                return board[i][0];
        }

        if (board[0][0] != 0 && board[0][0] == board[1][1] && board[0][0] == board[2][2])
            return board[0][0];
        if (board[2][0] != 0 && board[2][0] == board[1][1] && board[2][0] == board[0][2])
            return board[2][0];

        return 0;
    }

}