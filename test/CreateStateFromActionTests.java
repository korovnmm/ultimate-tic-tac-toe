import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CreateStateFromActionTests {

    // HAPPY PATH
    @Test
    void blankApplyUpperLeft() {
        int[][] input = new int[][]{
                {0, 0, 0},
                {0, 0, 0},
                {0, 0, 0},
        };
        int[][] newState = Player.createStateFromAction(1, "0 0", input);

        assertArrayEquals(new int[][] {
            {1, 0, 0},
            {0, 0, 0},
            {0, 0, 0},
        }, newState);
    }

    @Test
    void nonBlankApplyLowerRight() {
        int[][] input = new int[][]{
                {1, 0, 2},
                {0, 1, 0},
                {0, 0, 0},
        };
        int[][] newState = Player.createStateFromAction(2, "2 2", input);

        assertArrayEquals(new int[][] {
                {1, 0, 2},
                {0, 1, 0},
                {0, 0, 2},
        }, newState);
    }

    @Test
    void newInputStateIsCreated() {
        int[][] input = new int[][]{
                {1, 0, 2},
                {0, 1, 0},
                {0, 0, 0},
        };
        int[][] newState = Player.createStateFromAction(2, "2 2", input);
        System.out.println(newState.toString());
        assertNotSame(input, newState);

        assertArrayEquals(new int[][] {
                {1, 0, 2},
                {0, 1, 0},
                {0, 0, 0},
        }, input);
    }

    @Test
    void winningBoardAllowsModification() {
        int[][] input = new int[][]{
                {1, 0, 2},
                {0, 1, 0},
                {0, 0, 1},
        };
        int[][] newState = Player.createStateFromAction(2, "1 2", input);

        assertArrayEquals(new int[][] {
                {1, 0, 2},
                {0, 1, 2},
                {0, 0, 1},
        }, newState);
    }



    // SAD PATH
    @Test
    void nonEmptyCellThrowsException() {
        int[][] input = new int[][]{
                {1, 0, 2},
                {0, 1, 1},
                {0, 0, 1},
        };
        assertThrows(RuntimeException.class,
                () -> Player.createStateFromAction(2, "1 2", input));
    }

    @Test
    void outOfBoundsThrowsException() {
        int[][] input = new int[][]{
                {1, 0, 2},
                {0, 1, 1},
                {0, 0, 1},
        };
        assertThrows(RuntimeException.class,
                () -> Player.createStateFromAction(2, "1 3", input));
    }

    @Test
    void nonFormattedActionThrowsException() {
        int[][] input = new int[][]{
                {1, 0, 2},
                {0, 1, 1},
                {0, 0, 1},
        };
        assertThrows(RuntimeException.class,
                () -> Player.createStateFromAction(2, "asdf", input));
    }

    @Test
    void nullArgsThrowsException() {
        int[][] input = new int[][]{
                {1, 0, 2},
                {0, 1, 1},
                {0, 0, 1},
        };
        assertThrows(RuntimeException.class,
                () -> Player.createStateFromAction(2, "", input));
    }
}
