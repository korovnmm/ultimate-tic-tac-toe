import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.util.Random;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GetNextMoveTests {

    private Scanner buildScanner(int opponentRow, int opponentCol, String ... actions) {
        StringBuilder builder = new StringBuilder();
        builder.append(Integer.toString(opponentRow));
        builder.append(Integer.toString(opponentCol));
        builder.append(Integer.toString(actions.length));

        for(String action : actions) {
            builder.append(action);
        }

        return new Scanner(new ByteArrayInputStream(builder.toString().getBytes()));
    }

    @Test
    void randomMoveIsSelectedForNonWinningBoard() {
        int[][] board = new int[][]{
                {0, 0, 1},
                {2, 2, 0},
                {1, 0, 0},
        };
        Scanner in = buildScanner(-1, -1,
                       "0 0",
                                "1 2",
                                "2 1",
                                "2 2");
        Random rnd = new Random(100);
        String output = Player.getNextMove(in, rnd, board);

        assertEquals("1 2", output);

    }

    @Test
    void winningMoveIsSelectedIfOneMoveAway() {
        int[][] board = new int[][]{
                {0, 0, 1},
                {2, 2, 1},
                {1, 0, 0},
        };
        Scanner in = buildScanner(-1, -1,
                "0 0",
                "1 2",
                "2 1",
                "2 2");
        Random rnd = new Random(100);
        String output = Player.getNextMove(in, rnd, board);

        assertEquals("2 2", output);

    }
}
