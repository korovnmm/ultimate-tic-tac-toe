import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class GetWinnerTests {

    void assertWinner(int expectedWinner, int[][] board) {
        int actual = Player.getWinner(board);

        assertEquals(expectedWinner, actual);
    }

    @Test
    void emptyBoard() {
        assertWinner(0, new int[][]{
                {0, 0, 0},
                {0, 0, 0},
                {0, 0, 0}
        });
    }

    @Test
    void rightColumnWinnerPlayer2() {
        assertWinner(2, new int[][]{
                {1, 0, 2},
                {1, 0, 2},
                {0, 1, 2}
        });
    }

    @Test
    void middleColumnWinner() {
        assertWinner(1, new int[][]{
                {0, 1, 0},
                {0, 1, 0},
                {2, 1, 0}});
    }

    @Test
    void topRowWinner() {
        assertWinner(1, new int[][]{
                {1, 1, 1},
                {0, 2, 2},
                {2, 1, 2}});
    }

    @Test
    void middleRowWinnerPlayer2() {
        assertWinner(2, new int[][]{
                {1, 1, 0},
                {2, 2, 2},
                {2, 1, 2}});
    }

    @Test
    void bottomRowWinner() {
        assertWinner(1, new int[][]{
                {1, 1, 0},
                {0, 2, 2},
                {1, 1, 1}});
    }

    @Test
    void leftDiagonalWinnerPlayer2() {
        assertWinner(2, new int[][]{
                {2, 1, 0},
                {0, 2, 0},
                {1, 1, 2}});
    }

    @Test
    void rightDiagonalWinner() {
        assertWinner(1, new int[][]{
                {0, 0, 1},
                {0, 1, 2},
                {1, 2, 0}});
    }
}
